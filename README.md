Workflow Examples
=================

These are some simple workflow examples using the Pegasus Workflow Management
System.

Note that these examples are now shipped with Pegasus in share/pegasus/examples/simple-examples
